<?php 
    require "logica/Usuario.php";
    require "persistencia/Conexion.php";
    
    $usuario = new Usuario ($_GET["idUsu"]);
    $usuario -> consultar();
?>

<div class="modal-header">
	<h5 class="modal-title" id="exampleModalLabel"><?php echo $usuario -> getNombre() . " " . $usuario -> getApellido()?></h5>
</div>
<div class="modal-body"><img src="<?php echo $usuario -> getFoto()?>" width="100%" height="100%"></div>
<div class="modal-footer">
	<button type="button" class="btn btn-danger modal_btn3 btn-block" data-dismiss="modal">Cerrar</button>
</div>
