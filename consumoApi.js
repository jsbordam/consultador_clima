
//Capturo la ciudad que se esta consultando que viene de php:
const city = $("#city").val()
const $contenedor = document.getElementById('contenedor')

document.addEventListener('DOMContentLoaded',async () => 
{
    await initView();
});


const initView = async () => 
{
    //Url de la ciudad de consulta a la API:
    const url = "https://api.openweathermap.org/data/2.5/weather?q=" + city + "&appid=7ffd4978ca73746440968a9319ea4745"

    //Se genera los resultados de la API:

    const xhttp = new XMLHttpRequest()
    xhttp.open("GET", url)
    xhttp.send()

    xhttp.onreadystatechange = function () 
    {
        if (this.readyState === 4 && this.status === 200)
        {
            const data = JSON.parse(this.responseText)
            console.log(data);

            //Data que quiero mostrar:
            let icon
            const descripcion = data.weather[0].description;
            const coordenadas = "Latitude: " + data.coord.lat + " Longitude: " + data.coord.lon;
            const humedad = "humidity: " + data.main.humidity + "%";
            const presion = "pressure: " + data.main.pressure;

            const temperatura = "temp: " + Math.trunc(data.main.temp - 273.15) + "°C";

            //Inyección de HTML:
            loadHtml(descripcion, icon=1);
            loadHtml(coordenadas, icon=2);
            loadHtml(humedad, icon=3);
            loadHtml(presion, icon=4);
            loadHtml(temperatura, icon=5);
        } 
    }
}

const loadHtml = (value, icon) => 
{
    const $nodeDiv = document.createElement('div'); //<div>
    const $nodeI = document.createElement('i'); //<i>
    const $nodeH3 = document.createElement('h3'); //<h3>
    

    /* <div class="text-center">
            <i class=""></i>
            <h3></h3>
        </div> */
    $contenedor.appendChild($nodeDiv);
        $nodeDiv.setAttribute('class', 'text-center');
            $nodeDiv.appendChild($nodeI);
                if(icon === 1) 
                {
                    $nodeI.setAttribute('class', 'fas fa-cloud-sun');
                }
                else if (icon === 2)
                {
                    $nodeI.setAttribute('class', 'fas fa-map-marked-alt');
                }
                else if (icon === 3)
                {
                    $nodeI.setAttribute('class', 'fas fa-tint');
                }
                else if (icon === 4)
                {
                    $nodeI.setAttribute('class', 'fas fa-stopwatch');
                }
                else
                {
                    $nodeI.setAttribute('class', 'fas fa-temperature-low');
                }
                $nodeI.setAttribute('style', 'font-size:60px; color:#f30018;');
            $nodeDiv.appendChild($nodeH3);
                $nodeH3.innerHTML = value;
}