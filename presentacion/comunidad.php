<?php 
//Para controlar la seguridad de las paginas interiores, a las cuales solo se accede con logueo:
if (isset($_SESSION["id"]))
{   
?>
<?php 
    //Cuando quiere guardar un mensaje:
    if (isset($_POST["postear"]))
    {
        $mensaje = $_POST["Mensaje"];
        $fecha = date("Y/m/d - G:i:s");
        $idUsuario = $_SESSION["id"];

        //Se crea el registro:
        $msj = new Mensaje("", $mensaje, $fecha, $idUsuario);
        $msj->crear();
    }

    //Se consultan todos los mensajes:
    $m = new Mensaje();
    $registros = $m->consultarTodos();
?>


<div class="container">
    <div class="row mt-3">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h3>Comunidad de Usuarios</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <font face="cursive" color="#f30018">
                            <h4>* Deja un mensaje, cualquier usuario podra leerlo.</h4>
                        </font>
                    </div>
                    <br>
                    <form action=<?php echo "index.php?pid=" . base64_encode("presentacion/comunidad.php")?>
                        method="post">
                        <div class="form-group">
                            <input type="text" name="Mensaje" class="form-control" placeholder="Escribe algo"
                                required="required">
                        </div>

                        <div class="form-group">
                            <input type="submit" name="postear" value="Postear" class="btn login_btn btn-block">
                        </div>
                    </form>
                    <!-- Para avisar que se guardo el mensaje correctamente-->
                    <?php
                        if(isset($_POST["postear"]))
                        { 
                    ?>
                    <section id="alert2">
                        <div class="alert alert-dismissible fade show" role="alert">
                            <strong><i class="fas fa-check-circle"></i> Mensaje subido correctamente!</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </section>
                    <?php 
                        }
                    ?>
                    <br><br>
                    <div class="card">
                        <div class="card-header">
                            <h3>Registros de todos los usuarios</h3>
                        </div>
                        <div class="card-body">
                            <table class="table table-secondary table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center">
                                            <h5>
                                                <font face='Arial Black' size='3' color='black'>Registro</font>
                                            </h5>
                                        </th>
                                        <th class="text-center">
                                            <h5>
                                                <font face='Arial Black' size='3' color='black'>Usuario</font>
                                            </h5>
                                        </th>
                                        <th class="text-center">
                                            <h5>
                                                <font face='Arial Black' size='3' color='black'>Rol</font>
                                            </h5>
                                        </th>
                                        <th class="text-center">
                                            <h5>
                                                <font face='Arial Black' size='3' color='black'>Mensaje</font>
                                            </h5>
                                        </th>
                                        <th class="text-center">
                                            <h5>
                                                <font face='Arial Black' size='3' color='black'>Fecha</font>
                                            </h5>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        foreach ($registros as $registroActual)
                                        {    
                                            //Se consulta de que usuario es el mensaje:
                                            $usuario = new Usuario($registroActual->getIdUsuario());
                                            $usuario->consultar();     

                                            echo "<tr>";
                                                echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $registroActual->getIdMensaje() . "</font></h5></td>";             	        
                                                echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $usuario->getNombre() . " " . $usuario->getApellido(). "</font></h5></td>";
                                                
                                                //Para rol:
                                                if ($usuario->getIdRol() == 1)
                                                {
                                                    $rol = "<font color='red'>Admin</font>";
                                                }
                                                else
                                                {
                                                    $rol = "<font color='red'>Cliente</font>";
                                                }
                                                
                                                echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $rol . "</font></h5></td>";
                                                echo "<td><h5><font face='Arial' size='3' color='black'>" . $registroActual->getMensaje() . "</font></h5></td>";   
                                                echo "<td class='text-center'><h5><font face='Arial' size='3' color='blue'>" . $registroActual->getFecha() . "</font></h5></td>";                                             
                                            echo "</tr>";                	   
                                        }              
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php  
}
else //Si no existe sesion:
{
?>
<section id="alert1">
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong><i class="fas fa-exclamation-triangle"></i> ALERTA DE SEGURIDAD! No puede ingresar a esta sección, 
            primero inicie sesión con su usuario.</strong>
    </div>
</section>

<meta http-equiv="refresh" content="5;url=index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>" />
<?php
}
?>