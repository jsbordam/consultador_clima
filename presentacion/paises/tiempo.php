<?php 
//Para controlar la seguridad de las paginas interiores, a las cuales solo se accede con logueo:
if (isset($_SESSION["id"]))
{   
?>

<div class="container">
    <div class="row mt-3">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-9">
                            <h3>Resultados de API OpenWeather</h3>
                        </div>
                        <div class="col-md-3">
                            <a href=""
                                class='btn float-right login_btn2'>
                                <?php echo $_GET["name"];?>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <input id="city" value=<?php echo $_GET["name"];?> style="display: none;">

                    <div id="contenedor" class="clearfix">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="consumoApi.js"></script>

<?php  
}
else //Si no existe sesion:
{
?>
<section id="alert1">
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong><i class="fas fa-exclamation-triangle"></i> ALERTA DE SEGURIDAD! No puede ingresar a esta sección, 
            primero inicie sesión con su usuario.</strong>
    </div>
</section>

<meta http-equiv="refresh" content="5;url=index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>" />
<?php
}
?>