<?php 
//Para controlar la seguridad de las paginas interiores, a las cuales solo se accede con logueo:
if (isset($_SESSION["id"]))
{   
?>

<div class="container">
    <div class="row mt-3">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-9">
                            <h3>El tiempo en Estados Unidos</h3>
                        </div>
                        <div class="col-md-3">
                            <a href="index.php?pid=<?php echo base64_encode("presentacion/cliente/consultarPaises.php")?>"
                                class='btn float-right login_btn2'>
                                ATRAS
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Filadelfia">
                                    <img src="https://st3.depositphotos.com/29384342/33681/i/450/depositphotos_336810328-stock-photo-panoramic-picture-philadelphia-skyline-schuylkill.jpg" width="100%"
                                        height="250px">
                                </a>
                                <br>
                                <h3>Filadelfia</h3>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Chicago">
                                    <img src="https://lifestyle.americaeconomia.com/sites/lifestyle.americaeconomia.com/files/styles/600x600/public/chicago.jpg"
                                        width="100%" height="250px">
                                </a>
                                <br>
                                <h3>Chicago</h3>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Washington">
                                    <img src="https://st2.depositphotos.com/1053932/6590/i/600/depositphotos_65906365-stock-photo-washington-monument-capitol-and-lincoln.jpg"
                                        width="100%" height="250px">
                                </a>
                                <br>
                                <h3>Washington D.C</h3>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Miami">
                                    <img src="https://cdn-3.expansion.mx/dims4/default/846ad10/2147483647/strip/true/crop/1500x1500+0+0/resize/600x600!/quality/90/?url=https%3A%2F%2Fcherry-brightspot.s3.amazonaws.com%2Fab%2F6a%2Ff579ee914f6d9cd01dc3b654e273%2Fedificios.jpg" width="100%"
                                        height="250px">
                                </a>
                                <br>
                                <h3>Miami</h3>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Boston">
                                    <img src="https://st2.depositphotos.com/1053932/6930/i/450/depositphotos_69309643-stock-photo-boston-skyline-from-fan-pier.jpg"
                                        width="100%" height="250px">
                                </a>
                                <br>
                                <h3>Boston</h3>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Houston">
                                    <img src="https://st2.depositphotos.com/1053932/5777/i/600/depositphotos_57777379-stock-photo-houston-skyline-at-sunset-sabine.jpg"
                                        width="100%" height="250px">
                                </a>
                                <br>
                                <h3>Houston</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php  
}
else //Si no existe sesion:
{
?>
<section id="alert1">
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong><i class="fas fa-exclamation-triangle"></i> ALERTA DE SEGURIDAD! No puede ingresar a esta sección, 
            primero inicie sesión con su usuario.</strong>
    </div>
</section>

<meta http-equiv="refresh" content="5;url=index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>" />
<?php
}
?>