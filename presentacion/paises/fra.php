<?php 
//Para controlar la seguridad de las paginas interiores, a las cuales solo se accede con logueo:
if (isset($_SESSION["id"]))
{   
?>

<div class="container">
    <div class="row mt-3">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-9">
                            <h3>El tiempo en Francia</h3>
                        </div>
                        <div class="col-md-3">
                            <a href="index.php?pid=<?php echo base64_encode("presentacion/cliente/consultarPaises.php")?>"
                                class='btn float-right login_btn2'>
                                ATRAS
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Paris">
                                    <img src="https://a.storyblok.com/f/58806/600x600/aca34236ac/par_stage_mob.jpg" width="100%"
                                        height="250px">
                                </a>
                                <br>
                                <h3>Paris</h3>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Marsella">
                                    <img src="https://st3.depositphotos.com/1010305/16077/i/450/depositphotos_160778808-stock-photo-aerial-view-of-gibraltar-gibraltar.jpg"
                                        width="100%" height="250px">
                                </a>
                                <br>
                                <h3>Marsella</h3>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Lyon">
                                    <img src="https://st.depositphotos.com/1022400/2750/i/450/depositphotos_27502309-stock-photo-lyon-cityscape-from-saone-river.jpg"
                                        width="100%" height="250px">
                                </a>
                                <br>
                                <h3>Lyon</h3>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Montpellier">
                                    <img src="https://i.pinimg.com/474x/cc/0f/d9/cc0fd9aef52b705a03e71e2a345a10af.jpg" width="100%"
                                        height="250px">
                                </a>
                                <br>
                                <h3>Montpellier</h3>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Estrasburgo">
                                    <img src="https://st4.depositphotos.com/1673416/i/600/depositphotos_228526384-stock-photo-strasbourg-france-sentember-2018-old.jpg"
                                        width="100%" height="250px">
                                </a>
                                <br>
                                <h3>Estrasburgo</h3>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Niza">
                                    <img src="https://st.depositphotos.com/1592314/1376/i/600/depositphotos_13766914-stock-photo-view-of-mediterranean-resort-nice.jpg"
                                        width="100%" height="250px">
                                </a>
                                <br>
                                <h3>Niza</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php  
}
else //Si no existe sesion:
{
?>
<section id="alert1">
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong><i class="fas fa-exclamation-triangle"></i> ALERTA DE SEGURIDAD! No puede ingresar a esta sección, 
            primero inicie sesión con su usuario.</strong>
    </div>
</section>

<meta http-equiv="refresh" content="5;url=index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>" />
<?php
}
?>