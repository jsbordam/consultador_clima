<?php 
//Para controlar la seguridad de las paginas interiores, a las cuales solo se accede con logueo:
if (isset($_SESSION["id"]))
{   
?>

<div class="container">
    <div class="row mt-3">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-9">
                            <h3>El tiempo en España</h3>
                        </div>
                        <div class="col-md-3">
                            <a href="index.php?pid=<?php echo base64_encode("presentacion/cliente/consultarPaises.php")?>"
                                class='btn float-right login_btn2'>
                                ATRAS
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Madrid">
                                    <img src="https://www.collegiate-ac.es/propeller/uploads/sites/3/2019/11/florian-wehde-1092251-unsplash-600x600.jpg" width="100%"
                                        height="250px">
                                </a>
                                <br>
                                <h3>Madrid</h3>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Sevilla">
                                    <img src="https://www.skydivespain.com/wp-content/uploads/sevilla-600x600.jpg"
                                        width="100%" height="250px">
                                </a>
                                <br>
                                <h3>Sevilla</h3>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Barcelona">
                                    <img src="https://st2.depositphotos.com/6588682/9419/i/600/depositphotos_94190360-stock-photo-barcelona-city-in-spain.jpg"
                                        width="100%" height="250px">
                                </a>
                                <br>
                                <h3>Barcelona</h3>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Bilbao">
                                    <img src="https://static.barcelo.com/content/dam/bhg/master/es/hoteles/spain/pais-vasco/bilbao/barcelo-bilbao-nervion/galeria/fachada/BBIL_VIEW_03.jpg.bhgimg.square600.jpg/1604568151087.jpg" width="100%"
                                        height="250px">
                                </a>
                                <br>
                                <h3>Bilbao</h3>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Granada">
                                    <img src="https://st4.depositphotos.com/3321903/i/600/depositphotos_209808674-stock-photo-cityscape-granada-southern-spain-alhambra.jpg"
                                        width="100%" height="250px">
                                </a>
                                <br>
                                <h3>Granada</h3>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Valencia">
                                    <img src="https://st.depositphotos.com/1053932/3977/i/600/depositphotos_39770901-stock-photo-valencia-historic-downtown-el-miguelete.jpg"
                                        width="100%" height="250px">
                                </a>
                                <br>
                                <h3>Valencia</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php  
}
else //Si no existe sesion:
{
?>
<section id="alert1">
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong><i class="fas fa-exclamation-triangle"></i> ALERTA DE SEGURIDAD! No puede ingresar a esta sección, 
            primero inicie sesión con su usuario.</strong>
    </div>
</section>

<meta http-equiv="refresh" content="5;url=index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>" />
<?php
}
?>