<?php 
//Para controlar la seguridad de las paginas interiores, a las cuales solo se accede con logueo:
if (isset($_SESSION["id"]))
{   
?>

<div class="container">
    <div class="row mt-3">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-9">
                            <h3>El tiempo en China</h3>
                        </div>
                        <div class="col-md-3">
                            <a href="index.php?pid=<?php echo base64_encode("presentacion/cliente/consultarPaises.php")?>"
                                class='btn float-right login_btn2'>
                                ATRAS
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Pekin">
                                    <img src="https://st2.depositphotos.com/4794643/12378/i/450/depositphotos_123784254-stock-photo-chinese-beijing-forbidden-city.jpg" width="100%"
                                        height="250px">
                                </a>
                                <br>
                                <h3>Pekin</h3>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Shanghai">
                                    <img src="https://st.depositphotos.com/1006880/3999/i/600/depositphotos_39996585-stock-photo-shanghai-skyline-in-daytime.jpg"
                                        width="100%" height="250px">
                                </a>
                                <br>
                                <h3>Shanghai</h3>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Wuhan">
                                    <img src="https://fastly.4sqi.net/img/general/600x600/30550402_1f0IJlKSo1A9wTZ8KBmJrWMWXT7iFAC_nSOYp603rEA.jpg"
                                        width="100%" height="250px">
                                </a>
                                <br>
                                <h3>Wuhan</h3>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Chengdu">
                                    <img src="https://i.pinimg.com/originals/70/87/1e/70871ef18b9719b5e22647bc8b046a06.jpg" width="100%"
                                        height="250px">
                                </a>
                                <br>
                                <h3>Chengdu</h3>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Tianjin">
                                    <img src="https://st2.depositphotos.com/6096106/8861/i/600/depositphotos_88611056-stock-photo-beautiful-night-view-of-the.jpg"
                                        width="100%" height="250px">
                                </a>
                                <br>
                                <h3>Tianjin</h3>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Shenzhen">
                                    <img src="https://i.furniturehomewares.com/images/001/img-5858.jpg"
                                        width="100%" height="250px">
                                </a>
                                <br>
                                <h3>Shenzhen</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php  
}
else //Si no existe sesion:
{
?>
<section id="alert1">
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong><i class="fas fa-exclamation-triangle"></i> ALERTA DE SEGURIDAD! No puede ingresar a esta sección, 
            primero inicie sesión con su usuario.</strong>
    </div>
</section>

<meta http-equiv="refresh" content="5;url=index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>" />
<?php
}
?>