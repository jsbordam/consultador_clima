<?php 
//Para controlar la seguridad de las paginas interiores, a las cuales solo se accede con logueo:
if (isset($_SESSION["id"]))
{   
?>

<div class="container">
    <div class="row mt-3">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-9">
                            <h3>El tiempo en Italia</h3>
                        </div>
                        <div class="col-md-3">
                            <a href="index.php?pid=<?php echo base64_encode("presentacion/cliente/consultarPaises.php")?>"
                                class='btn float-right login_btn2'>
                                ATRAS
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Roma">
                                    <img src="https://conocedeculturas.com/wp-content/uploads/2020/07/CULTURA-ROMANA.jpg" width="100%"
                                        height="250px">
                                </a>
                                <br>
                                <h3>Roma</h3>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Florencia">
                                    <img src="https://m.visittuscany.com/shared/make/immagini/tramonti-firenze_q6G.jpeg?__scale=w:600,h:600,t:2,q:85"
                                        width="100%" height="250px">
                                </a>
                                <br>
                                <h3>Florencia</h3>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Venecia">
                                    <img src="https://i.pinimg.com/originals/b4/a3/bb/b4a3bba799eb72d2d57f8eb5a6a56434.jpg"
                                        width="100%" height="250px">
                                </a>
                                <br>
                                <h3>Venecia</h3>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Milan">
                                    <img src="https://st2.depositphotos.com/36511666/43854/i/450/depositphotos_438545142-stock-photo-milan-street-panorama-milan-trams.jpg" width="100%"
                                        height="250px">
                                </a>
                                <br>
                                <h3>Milán</h3>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Turin">
                                    <img src="https://fastly.4sqi.net/img/general/600x600/13789611_PJGq9EEKZOMIX1f0hP45bqyyMEV33EaRhrCi7Z_9BsI.jpg"
                                        width="100%" height="250px">
                                </a>
                                <br>
                                <h3>Turín</h3>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Bolonia">
                                    <img src="https://fastly.4sqi.net/img/general/600x600/33729256__fdkrO7tPrRYzJ0vB6QfcVSStu57Wj4aWvizkS_znuI.jpg"
                                        width="100%" height="250px">
                                </a>
                                <br>
                                <h3>Bolonia</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php  
}
else //Si no existe sesion:
{
?>
<section id="alert1">
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong><i class="fas fa-exclamation-triangle"></i> ALERTA DE SEGURIDAD! No puede ingresar a esta sección, 
            primero inicie sesión con su usuario.</strong>
    </div>
</section>

<meta http-equiv="refresh" content="5;url=index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>" />
<?php
}
?>