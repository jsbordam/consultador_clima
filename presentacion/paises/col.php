<?php 
//Para controlar la seguridad de las paginas interiores, a las cuales solo se accede con logueo:
if (isset($_SESSION["id"]))
{   
?>

<div class="container">
    <div class="row mt-3">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-9">
                            <h3>El tiempo en Colombia</h3>
                        </div>
                        <div class="col-md-3">
                            <a href="index.php?pid=<?php echo base64_encode("presentacion/cliente/consultarPaises.php")?>"
                                class='btn float-right login_btn2'>
                                ATRAS
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Bogota">
                                    <img src="https://contextomedia.com/wp-content/uploads/bogota-1-1.jpg" width="100%"
                                        height="250px">
                                </a>
                                <br>
                                <h3>Bogota D.C</h3>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Medellin">
                                    <img src="https://colombialovelytravel.com/wp-content/uploads/2019/03/medellin-flores-cafe-gal-1.jpg"
                                        width="100%" height="250px">
                                </a>
                                <br>
                                <h3>Medellin</h3>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Cartagena">
                                    <img src="https://albitourscartagena.com/wp-content/uploads/2016/06/City-Tours-cartagena-04-min.jpg"
                                        width="100%" height="250px">
                                </a>
                                <br>
                                <h3>Cartagena</h3>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Barranquilla">
                                    <img src="https://pbs.twimg.com/media/CCAGqyRXIAAmiBN.jpg" width="100%"
                                        height="250px">
                                </a>
                                <br>
                                <h3>Barranquilla</h3>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Cali">
                                    <img src="https://www.elpais.com.co/elpais/sites/default/files/de-cali-se-habla-bien/juan_camilo_medina.jpg"
                                        width="100%" height="250px">
                                </a>
                                <br>
                                <h3>Cali</h3>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/tiempo.php")?>&name=Bucaramanga">
                                    <img src="https://fastly.4sqi.net/img/general/600x600/16414657_va3-LTvB68-jqRLj_vuP9dLSAx2Ye6ZnX9fwVVtWn9E.jpg"
                                        width="100%" height="250px">
                                </a>
                                <br>
                                <h3>Bucaramanga</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php  
}
else //Si no existe sesion:
{
?>
<section id="alert1">
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong><i class="fas fa-exclamation-triangle"></i> ALERTA DE SEGURIDAD! No puede ingresar a esta sección, 
            primero inicie sesión con su usuario.</strong>
    </div>
</section>

<meta http-equiv="refresh" content="5;url=index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>" />
<?php
}
?>