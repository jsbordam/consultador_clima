<?php 
//Para controlar la seguridad de las paginas interiores, a las cuales solo se accede con logueo:
if (isset($_SESSION["id"]))
{   
?>
<div class="container">
    <div class="row mt-3">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h3>Paises Disponibles</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/col.php")?>">
                                    <img src="img/col.png" width="200px" height="200px">
                                </a>
                                <br><br>
                                <h3>COLOMBIA</h3>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/chi.php")?>">
                                    <img src="img/chi.jpg" width="200px" height="200px">
                                </a>
                                <br><br>
                                <h3>CHINA</h3>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/esp.php")?>">
                                    <img src="img/esp.png" width="200px" height="200px">
                                </a>
                                <br><br>
                                <h3>ESPAÑA</h3>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/eu.php")?>">
                                    <img src="img/eu.jpg" width="200px" height="200px">
                                </a>
                                <br><br>
                                <h3>ESTADOS UNIDOS</h3>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/fra.php")?>">
                                    <img src="img/fra.jpg" width="200px" height="200px">
                                </a>
                                <br><br>
                                <h3>FRANCIA</h3>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card" style="text-align: center;">
                                <a href="index.php?pid=<?php echo base64_encode ("presentacion/paises/ita.php")?>">
                                    <img src="img/ita.jpg" width="200px" height="200px">
                                </a>
                                <br><br>
                                <h3>ITALIA</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php  
}
else //Si no existe sesion:
{
?>
<section id="alert1">
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong><i class="fas fa-exclamation-triangle"></i> ALERTA DE SEGURIDAD! No puede ingresar a esta sección, 
            primero inicie sesión con su usuario.</strong>
    </div>
</section>

<meta http-equiv="refresh" content="5;url=index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>" />
<?php
}
?>