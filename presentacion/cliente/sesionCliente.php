<?php
    $cliente = new Usuario($_SESSION["id"]);
    $cliente -> consultar();
?>
<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<div class="card-header">
					<h3>Bienvenido</h3>
				</div>
				<div class="card-body">
					<div class="text-center">
						<font face="cursive" size="5px" color="blue">
							Cliente:<h2><?php echo $cliente -> getNombre() . " " . $cliente -> getApellido();?></h2>
						</font>
					</div>
				</div>
			
				<div class="text-center">
					<img src="<?php echo $cliente -> getFoto()?>" width="400px" heigh="400px"/>
				</div>
			</div>
		</div>
	</div>
</div>
