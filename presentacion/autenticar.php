<?php
    $correo = $_POST["correo"];
    $clave = $_POST["clave"];
    
    $usuario = new Usuario("", "", "", "", "", $correo, $clave, "", "", "");
    
    //Validamos si se encontro ese Ususario en la BD, independientemente del rol:
    
    if ($usuario -> autenticar())// Si retorna verdadero
    {
        //En la variable session se guarda el id del usuario que se autentica:
        $_SESSION["id"] = $usuario -> getIdUsuario();
        
        //Ahora se le asigna el rol que corresponda segun los datos de usuario:
        if ($usuario -> getIdRol() == 1)
        {
            //Rol 1 = ADMINISTRADOR
            $_SESSION["rol"] = "Administrador";
            
            //Como es ADMIN no tiene que hacer ninguna otra validación 
            //Entonces se redirecciona a la sesion Admin:
            header ("location: index.php?pid=" . base64_encode("presentacion/administrador/sesionAdministrador.php"));
        }
        else
        {
            //Rol 2 = CLIENTE
            $_SESSION["rol"] = "Cliente";
            
            //Ahora validamos su estado:
            if ($usuario -> getEstado() == 1)
            {
                //Su estado es 1 = HABILITADO
                //Entonces se redirecciona a la sesion Cliente:
                header ("location: index.php?pid=" . base64_encode("presentacion/cliente/sesionCliente.php"));
            }
            else 
            {
                //Su estado es 0 = DESHABILITADO
                //Error 2: Usuario deshabilitado
                //Entonces se redirecciona de nuevo a iniciar sesion con un error = 2:
                $_SESSION["id"] = null; //Se rompe la sesión porque no puede entrar
                header ("location: index.php?pid=" . base64_encode("presentacion/inicio.php") . "&error=2");
            }
        }   
    }
    else
    {
        //Error 1: Correo o clave incorrectos
        //Entonces se redirecciona de nuevo a iniciar sesion con un error 1:

        header ("location: index.php?pid=" . base64_encode("presentacion/inicio.php") . "&error=1");
    }
?>

