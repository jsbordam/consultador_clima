<?php 
    $administrador = new Usuario($_SESSION["id"]);
    $administrador -> consultar();
?>


<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="index.php?pid=<?php echo base64_encode ("presentacion/administrador/sesionAdministrador.php")?>"><i class="fas fa-home" data-toggle='tooltip' data-placement='bottom' title='Inicio'></i></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">   
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Usuarios
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode ("presentacion/administrador/crearUsuarios.php")?>">Crear</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode ("presentacion/administrador/consultarUsuarios.php")?>">Consultar</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Buscar</a>
        </div>
      </li>
      
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <font size="4px"><i class="fas fa-cloud-sun-rain"></i></font> Tiempo hoy
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode ("presentacion/cliente/consultarPaises.php")?>">Consultar paises</a>
        </div>
      </li>
      <li class="nav-item">
      	<a class="nav-link" href="index.php?pid=<?php echo base64_encode ("presentacion/comunidad.php")?>">Comunidad</a>
      </li>
    </ul>
    
    <ul class="navbar-nav"> 
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-user"></i> <?php echo $administrador -> getNombre() . " " . $administrador -> getApellido(); ?>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Editar perfil</a>
          <a class="dropdown-item" href="#">Editar foto</a>
        </div>        
      </li>
      <li class="nav-item">
      	<a class="nav-link" href="index.php?session=0">Cerrar Sesion</a>
      </li>
    </ul>
  </div>
</nav>