<?php
if (isset($_SESSION["id"]))
{
    if ($_SESSION["rol"] == "Administrador") //Funcion solo para admi
    {
?>
<?php
    if (isset($_GET["eliminar"]))
    {
        //Se elimina el usuario de la BD:
        $idUsuario = $_GET["idUsu"];
        $usuario = new Usuario($idUsuario);
        $usuario->eliminar();
    }

    //Consulto la lista de todos los usuarios:
    $usuario = new Usuario();
    $usuarios = $usuario -> consultarTodos();
?>


<div class="container">
    <div class="row mt-3">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h3>Usuarios registrados en el sistema</h3>
                </div>

                <div class="card-body">
                    <table class="table table-warning table-striped table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">
                                    <h5>
                                        <font face='Arial Black' size='3' color='black'>Codigo</font>
                                    </h5>
                                </th>
                                <th class="text-center">
                                    <h5>
                                        <font face='Arial Black' size='3' color='black'>Nombre</font>
                                    </h5>
                                </th>
                                <th class="text-center">
                                    <h5>
                                        <font face='Arial Black' size='3' color='black'>Apellido</font>
                                    </h5>
                                </th>
                                <th class="text-center">
                                    <h5>
                                        <font face='Arial Black' size='3' color='black'>Correo</font>
                                    </h5>
                                </th>
                                <th class="text-center">
                                    <h5>
                                        <font face='Arial Black' size='3' color='black'>Rol</font>
                                    </h5>
                                </th>
                                <th class="text-center">
                                    <h5>
                                        <font face='Arial Black' size='3' color='black'>Estado</font>
                                    </h5>
                                </th>
                                <th class="text-center">
                                    <h5>
                                        <font face='Arial Black' size='3' color='black'>Foto</font>
                                    </h5>
                                </th>
                                <th class="text-center">
                                    <h5>
                                        <font face='Arial Black' size='3' color='black'>Acciones</font>
                                    </h5>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
						foreach ($usuarios as $usuarioActual)
						{                  	   
							echo "<tr>";
								echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $usuarioActual->getIdUsuario() . "</font></h5></td>";             	        
								echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $usuarioActual->getNombre() . "</font></h5></td>";
								echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $usuarioActual->getApellido() . "</font></h5></td>";
								echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $usuarioActual->getCorreo() . "</font></h5></td>";
								
								//Para rol:
								if ($usuarioActual->getIdRol() == 1)
								{
									$rol = "Admin";
								}
								else
								{
									$rol = "Cliente";
								}
								
								echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $rol . "</font></h5></td>";
								echo "<td class='text-center'><h5><font face='Arial' size='5' color='black'><div id='estado" . $usuarioActual -> getIdUsuario() . "'>" . (($usuarioActual -> getEstado() == 1)?"<i class='fas fa-user-check' data-toggle='tooltip' data-placement='bottom' title='Habilitado'></i>":"<i class='fas fa-user-times' data-toggle='tooltip' data-placement='bottom' title='Deshabilitado'></i>") . "</font></h5></td>";
								echo "<td class='text-center'><a href='modalFoto.php?idUsu=". $usuarioActual -> getIdUsuario() ."' data-toggle='modal' data-target='#modalFoto'><font face='Arial' size='5' color='black'><i class='fas fa-eye' data-toggle='tooltip' data-placement='bottom' title='Ver Foto'></i></font></a></td>"; 
								echo "<td class='text-center'><a href='#'><font size='5' color='red'><i id='cambiarEstado" . $usuarioActual -> getIdUsuario() . "' class='fas fa-user-edit' data-toggle='tooltip' data-placement='bottom' title='Cambiar Estado'></i></font></a>
                                        <a href='index.php?pid=" . base64_encode("presentacion/administrador/editarUsuarios.php") . "&idUsu=" . $usuarioActual -> getIdUsuario() . "'><font size='5' color='red'><i class='fas fa-edit' data-toggle='tooltip' data-placement='bottom' title='Editar'></i></font></a>
                                        <a href='modalConfirmacion.php?idUsu=". $usuarioActual -> getIdUsuario() ."' data-toggle='modal' data-target='#modalConfirm'><font size='5' color='red'><i class='fas fa-trash-alt' data-toggle='tooltip' data-placement='bottom' title='Eliminar'></i></font></a>
                                     </td>";
							echo "</tr>";                	   
						}              
            		?>
                        </tbody>
                    </table>

                    <div class="row justify-content-center grid-group-md text-xl-center">
                        <div class="row">
                            <div class="col-md-12 col-xl-12">
                                <div class="group offset-top-50">
                                    <a href="index.php?pid=<?php echo base64_encode("presentacion/administrador/consultarUsuarios.php")?>"
                                        class='btn float-right login_btn2'>
                                        ACTUALIZAR
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--Este es el Modal de Ver Foto: -->

    <section id="modalF">
        <div class="modal fade" id="modalFoto" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content" id="modal-content"></div>
            </div>
        </div>
    </section>

    <!--Este JavaScript es para el Modal de Foto: -->
    <script>
    $('body').on('show.bs.modal', '.modal', function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
    </script>

    <!--Este es el Modal de confirmacion de eliminar: -->

    <section id="modalC">
        <div class="modal fade" id="modalConfirm" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content" id="modal-content"></div>
            </div>
        </div>
    </section>

    <!--Este JavaScript es para confirmacion: -->
    <script>
    $('body').on('show.bs.modal', '.modal', function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
    </script>


    <!--Este es el JQuery para cambiar estado: -->
    <script>
    $(document).ready(function() {
        <?php 
            foreach ($usuarios as $usuarioActual)
            {
                echo "$(\"#cambiarEstado". $usuarioActual -> getIdUsuario() ."\").click(function()\n";
                echo "{\n";
                //Definimos la ruta en donde se recargara esta capa y la encriptamos respectivamente:
                echo "    url = \"indexAjax.php?pid=" . base64_encode("presentacion/administrador/cambiarEstadoUsuarioAjax.php") . "&idUsu=" . $usuarioActual -> getIdUsuario() . "\"\n";
                echo "    $(\"#estado" . $usuarioActual -> getIdUsuario() . "\").load(url);\n";
                echo "});\n\n";
            }                  
        ?>
    });
    </script>

<?php 
    }
    else
    {
        echo "<section id='alert1'>";
            echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'>";
                echo "<strong><i class='fas fa-exclamation-triangle'></i> ALERTA DE SEGURIDAD! No tiene permisos para entrar a esta seccion...</strong>";
            echo "</div>";
        echo "</section>";
    }
}
else //Si no existe sesion:
{
?>
    <section id="alert1">
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong><i class="fas fa-exclamation-triangle"></i> ALERTA DE SEGURIDAD! No tiene permisos para entrar a esta seccion...</strong>
        </div>
    </section>
 
    <meta http-equiv="refresh" content="4;url=index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>" />
<?php
}
?>