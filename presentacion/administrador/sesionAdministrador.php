<?php
    $administrador = new Usuario($_SESSION["id"]);
    $administrador -> consultar();
?>
<div class="container">
    <div class="row mt-3">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h3>Bienvenido</h3>
                </div>
                <div class="card-body">
                    <div class="text-center">
                        <font face="cursive" size="5px" color="blue">
                            Administrador:<h2><?php echo $administrador -> getNombre() . " " . $administrador -> getApellido();?></h2>
                        </font>
                    </div>
                </div>

                <div class="text-center">
                    <img src="<?php echo $administrador -> getFoto()?>" width="400px" heigh="400px" />
                </div>
            </div>
        </div>
    </div>
</div>