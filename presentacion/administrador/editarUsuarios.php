<?php 
if (isset($_SESSION["id"]))
{   
    if ($_SESSION["rol"] == "Administrador") //Funcion solo para admi
    {
?>
<?php   
    //Para consultar los datos del usuario que quiere editar:

    $idUsuario = $_GET["idUsu"];

    if (isset($_POST["guardar"]))
    {
        $error = 0;
        //Recojo los datos que llegan por POST del formulario:
        $nombre = $_POST["nombre"];
        $apellido = $_POST["apellido"];
        
        //Para opcion tipo_docto:
        if ($_POST["tipo_docto"] == "Cedula de Ciudadania")
        {
            $tipo_docto = 0;
        }
        else if ($_POST["tipo_docto"] == "Tarjeta de Identidad")
        {
            $tipo_docto = 1;
        }
        else
        {
            $tipo_docto = 2; //Carnet de Extranjeria
        }
        
        $num_docto = $_POST["num_docto"];
        $clave = md5 ($_POST["clave"]); //Encriptamos la clave con MD5

        //Para opcion estado:
        if ($_POST["estado"] == "Habilitado")
        {
            $estado = 1; //Habilitado
        }
        else
        {
            $estado = 0; //Deshabilitado
        }
        
        //////////////////////////////////////////////////////////////////////////////////
        //Para el archivo de foto:
        
        $foto = $_FILES["foto"];
        $tipo = $foto ["type"];
        
        if ($tipo == "image/jpeg" || $tipo == "image/png")
        {
        
            //Esta sera la direccion final que tendra la foto para que se guarde en la carpeta
            //del proyecto:
            
            //Se le da nombre con time() para que guarde la fecha y la hora excata en que cambia su foto
            //De esta manera se controla que no hayan fotos con el mismo nombre:
            
            $urlServidor = "img/usuarios/" . time() . ".jpg"; 
            $urlLocal = $foto ["tmp_name"];// tmp_name es el nombre temporal que tiene ese archivo en el htdocs
            
            copy($urlLocal, $urlServidor); //Copie la imagen de local a servidor
        }
        else
        {
            $error = 1;
        }
        
        //////////////////////////////////////////////////////////////////////////////////
        
        //Para el rol con el que se registra:
        if ($_POST["idRol"] == "Cliente")
        {
            $idRol = 2;
        }
        else
        {
            $idRol = 1; //Admin

        }
        
        if ($error == 0)
        {
            //Edito el usuario:
            $usuario = new Usuario ($idUsuario, $nombre, $apellido, $tipo_docto, $num_docto, "", $clave, $estado, $urlServidor, $idRol);
            $usuario -> editar();
        }     
    }  
    
    $usuario = new Usuario ($idUsuario);
    $usuario -> consultar();
?>

<div class="container">
    <div class="row mt-3">
        <div class="col">
            <section id="edit">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-9">
                                <h3>Editando datos de usuario</h3>
                            </div>
                            <div class="col-md-3">
                                <a href="index.php?pid=<?php echo base64_encode("presentacion/administrador/consultarusuarios.php")?>"
                                    class='btn float-right login_btn2'>
                                    ATRAS
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <!-- Para avisar que se edito correctamente-->
                        <?php
                            if(isset($_POST["guardar"]) && $error == 0)
                            { 
                        ?>
                                <section id="alert2">
                                    <div class="alert alert-dismissible fade show" role="alert">
                                        <strong><i class="fas fa-check-circle"></i> Información modificada correctamente!</strong>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                </section>
                        <?php 
                            }
                        ?>

                        <!-- Para reporatar error en el archivo de foto-->
                        <?php
                            if (isset($_POST["guardar"]) && $error == 1)
                            { 
                        ?>
                                <section id="alert1">
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <strong><i class="fas fa-exclamation-triangle"></i> Error. El archivo foto debe ser de
                                            tipo: (jpg o png)</strong>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                </section>
                        <?php 
                            }               
                        ?>

                        <form
                            action=<?php echo "index.php?pid=" . base64_encode("presentacion/administrador/editarUsuarios.php") . "&idUsu=" . $_GET["idUsu"] . "&editar"?>
                            method="post" enctype="multipart/form-data">

                            <div class="form-row">
                                <div class="form-group col-sm-4">
                                    <label>Nombre:</label> <input type="text" name="nombre" class="form-control"
                                        placeholder="Nuevo nombre" value="<?php echo $usuario->getNombre()?>"
                                        required="required">
                                </div>
                                <div class="form-group col-sm-4">
                                    <label>Apellido:</label> <input type="text" name="apellido" class="form-control"
                                        placeholder="Nuevo apellido" value="<?php echo $usuario->getApellido()?>"
                                        required="required">
                                </div>
                                <div class="form-group col-sm-4">
                                    <label for="inputState">Tipo Documento:</label> <select id="inputState"
                                        class="form-control" name="tipo_docto">
                                        <option>Cedula de Ciudadania</option>
                                        <option>Tarjeta de Identidad</option>
                                        <option>Carnet de Extranjeria</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-sm-4">
                                    <label>Num Documento:</label> <input type="number" name="num_docto"
                                        class="form-control" placeholder="Nuevo num documento"
                                        value="<?php echo $usuario->getNum_docto()?>" required="required">
                                </div>
                                <div class="form-group col-sm-4">
                                    <label>Correo:</label> <input type="email" name="correo" class="form-control"
                                        disabled="disabled" value="<?php echo $usuario->getCorreo()?>"
                                        required="required">
                                </div>
                                <div class="form-group col-sm-4">
                                    <label>Clave:</label> <input type="password" name="clave" class="form-control"
                                        placeholder="Nueva clave" required="required">
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-sm-4">
                                    <label for="inputState">Estado:</label> <select id="inputState" class="form-control"
                                        name="estado">
                                        <option>Habilitado</option>
                                        <option>Dehabilitado</option>
                                    </select>
                                </div>

                                <div class="form-group col-sm-4">
                                    <label>Foto:</label>
                                    <div class="form-group">
                                        <input type="file" name="foto" class="form-control-file" required="required">
                                    </div>
                                </div>

                                <div class="form-group col-sm-4">
                                    <label for="inputState">Rol:</label> <select id="inputState" class="form-control"
                                        name="idRol">
                                        <option>Cliente</option>
                                        <option>Administrador</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <input type="submit" name="guardar" value="Guardar" class="btn login_btn btn-block">
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<?php 
    }
    else
    {
        echo "<section id='alert1'>";
            echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'>";
                echo "<strong><i class='fas fa-exclamation-triangle'></i> ALERTA DE SEGURIDAD! No tiene permisos para entrar a esta seccion...</strong>";
            echo "</div>";
        echo "</section>";
    }
}
else //Si no existe sesion:
{
?>
<section id="alert1">
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong><i class="fas fa-exclamation-triangle"></i> ALERTA DE SEGURIDAD! No tiene permisos para entrar a esta
            seccion...</strong>
    </div>
</section>

<meta http-equiv="refresh" content="4;url=index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>" />
<?php
}
?>