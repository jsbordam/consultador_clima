<?php 
if (isset($_SESSION["id"]))
{   
    if ($_SESSION["rol"] == "Administrador") //Funcion solo para admi
    {
?>
<?php   

    if (isset($_POST["crear"]))
    {
        $error = 0;
        //Recojo los datos que llegan por POST del formulario:
        $nombre = $_POST["nombre"];
        $apellido = $_POST["apellido"];
        
        //Para opcion tipo_docto:
        if ($_POST["tipo_docto"] == "Cedula de Ciudadania")
        {
            $tipo_docto = 0;
        }
        else if ($_POST["tipo_docto"] == "Tarjeta de Identidad")
        {
            $tipo_docto = 1;
        }
        else
        {
            $tipo_docto = 2; //Carnet de Extranjeria
        }
        
        $num_docto = $_POST["num_docto"];
        $correo = $_POST["correo"];
        $clave = md5 ($_POST["clave"]); //Encriptamos la clave con MD5

        //Para opcion estado:
        if ($_POST["estado"] == "Habilitado")
        {
            $estado = 1; //Habilitado
        }
        else
        {
            $estado = 0; //Deshabilitado
        }
        
        //////////////////////////////////////////////////////////////////////////////////
        //Para el archivo de foto:
        
        $foto = $_FILES["foto"];
        $tipo = $foto ["type"];
        
        if ($tipo == "image/jpeg" || $tipo == "image/png")
        {
        
            //Esta sera la direccion final que tendra la foto para que se guarde en la carpeta
            //del proyecto:
            
            //Se le da nombre con time() para que guarde la fecha y la hora excata en que cambia su foto
            //De esta manera se controla que no hayan fotos con el mismo nombre:
            
            $urlServidor = "img/usuarios/" . time() . ".jpg"; 
            $urlLocal = $foto ["tmp_name"];// tmp_name es el nombre temporal que tiene ese archivo en el htdocs
            
            copy($urlLocal, $urlServidor); //Copie la imagen de local a servidor
        }
        else
        {
            $error = 1;
        }
        
        //////////////////////////////////////////////////////////////////////////////////
        
        //Para el rol con el que se registra:
        if ($_POST["idRol"] == "Cliente")
        {
            $idRol = 2;
        }
        else
        {
            $idRol = 1; //Admin

        }
        
        if ($error == 0)
        {
            //Edito el usuario:
            $usuario = new Usuario ("", $nombre, $apellido, $tipo_docto, $num_docto, $correo, $clave, $estado, $urlServidor, $idRol);
            $usuario -> crear();
        }     
    }  
?>

<div class="container">
    <div class="row mt-3">
        <div class="col">
            <section id="edit">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-9">
                                <h3>Creando Usuario</h3>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <!-- Para avisar que se creo correctamente-->
                        <?php
                            if(isset($_POST["crear"]) && $error == 0)
                            { 
                        ?>
                        <section id="alert2">
                            <div class="alert alert-dismissible fade show" role="alert">
                                <strong><i class="fas fa-check-circle"></i> Usuario creado correctamente!</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </section>
                        <?php 
                            }
                        ?>

                        <!-- Para reporatar error en el archivo de foto-->
                        <?php
                            if (isset($_POST["crear"]) && $error == 1)
                            { 
                        ?>
                        <section id="alert1">
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <strong><i class="fas fa-exclamation-triangle"></i> Error. El archivo foto debe ser de
                                    tipo: (jpg o png)</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </section>
                        <?php 
                            }               
                        ?>

                        <form
                            action=<?php echo "index.php?pid=" . base64_encode("presentacion/administrador/crearUsuarios.php")?>
                            method="post" enctype="multipart/form-data">

                            <div class="form-row">
                                <div class="form-group col-sm-4">
                                    <label>Nombre:</label> <input type="text" name="nombre" class="form-control"
                                        placeholder="Escribe tu nombre" required="required">
                                </div>
                                <div class="form-group col-sm-4">
                                    <label>Apellido:</label> <input type="text" name="apellido" class="form-control"
                                        placeholder="Escribe tu apellido" required="required">
                                </div>
                                <div class="form-group col-sm-4">
                                    <label for="inputState">Tipo Documento:</label> <select id="inputState"
                                        class="form-control" name="tipo_docto">
                                        <option>Cedula de Ciudadania</option>
                                        <option>Tarjeta de Identidad</option>
                                        <option>Carnet de Extranjeria</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-sm-4">
                                    <label>Num Documento:</label> <input type="number" name="num_docto"
                                        class="form-control" placeholder="Escribe tu documento" required="required">
                                </div>
                                <div class="form-group col-sm-4">
                                    <label>Correo:</label> <input type="email" name="correo" class="form-control"
                                        placeholder="Escribe tu correo" required="required">
                                </div>
                                <div class="form-group col-sm-4">
                                    <label>Clave:</label> <input type="password" name="clave" class="form-control"
                                        placeholder="Escribe tu clave" required="required">
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-sm-4">
                                    <label for="inputState">Estado:</label> <select id="inputState" class="form-control"
                                        name="estado">
                                        <option>Habilitado</option>
                                        <option>Deshabilitado</option>
                                    </select>
                                </div>

                                <div class="form-group col-sm-4">
                                    <label>Foto:</label>
                                    <div class="form-group">
                                        <input type="file" name="foto" class="form-control-file" required="required">
                                    </div>
                                </div>

                                <div class="form-group col-sm-4">
                                    <label for="inputState">Rol:</label> <select id="inputState" class="form-control"
                                        name="idRol">
                                        <option>Cliente</option>
                                        <option>Administrador</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <input type="submit" name="crear" value="Crear"
                                    class="btn login_btn btn-block">
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<?php 
    }
    else
    {
        echo "<section id='alert1'>";
            echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'>";
                echo "<strong><i class='fas fa-exclamation-triangle'></i> ALERTA DE SEGURIDAD! No tiene permisos para entrar a esta seccion...</strong>";
            echo "</div>";
        echo "</section>";
    }
}
else //Si no existe sesion:
{
?>
<section id="alert1">
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong><i class="fas fa-exclamation-triangle"></i> ALERTA DE SEGURIDAD! No tiene permisos para entrar a esta
            seccion...</strong>
    </div>
</section>

<meta http-equiv="refresh" content="4;url=index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>" />
<?php
}
?>