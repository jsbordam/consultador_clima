<?php 
    $usuario = new Usuario ($_GET["idUsu"]);
    
    //Primero consulto el estado actual del usuario:
    $usuario -> consultar();

    
    //Luego le cambio el estado:
    if ($usuario -> getEstado() == 1)
    {
        $usuario -> cambiarEstado(0);
        echo "<h5><font face='Arial' size='5' color='black'><i class='fas fa-user-times' data-toggle='tooltip' data-placement='bottom' title='Deshabilitado'></i></font></h5>";
    }
    else 
    {
        $usuario -> cambiarEstado(1);
        echo "<h5><font face='Arial' size='5' color='black'><i class='fas fa-user-check' data-toggle='tooltip' data-placement='bottom' title='Habilitado'></i></font></h5>";
    }     
?>