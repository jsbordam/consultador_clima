<?php 
    $error = 0;
    if(isset($_GET["error"]))
    { 
        $error = $_GET["error"];
    }
?>

<div class="container">
    <?php include "presentacion/encabezado.php";?>
    <div class="row mt-5">
        <div class="col-lg-7">
            <div class="card">
                <div class="card-header">
                    <h3>Consultador de tiempo</h3>
                </div>
                <div class="card-body">
                    <img src="img/1.jpg" width="100%">
                </div>
            </div>
        </div>

        <div class="col-lg-5">
            <div class="card">
                <div class="card-header">
                    <h3>Autenticación</h3>
                </div>
                <div class="card-body">
                    <?php
                            if($error == 1)
                            { 
                        ?>
                        <section id="alert1">
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <strong><i class="fas fa-exclamation-triangle"></i> Correo o clave incorrectos. Vuelve a intentar</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </section>
                    <?php
                            }
                            else if($error == 2)
                            { 
                        ?>
                        <section id="alert1">
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <strong><i class="fas fa-exclamation-triangle"></i> Usuario deshabilitado. No puedes ingresar.</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </section>
                    <?php }?>
                    <form action=<?php echo "index.php?pid=" . base64_encode("presentacion/autenticar.php")?>
                        method="post">
                        <div class="form-group">
                            <input type="email" name="correo" class="form-control" placeholder="Correo"
                                required="required">
                        </div>
                        <div class="form-group">
                            <input type="password" name="clave" class="form-control" placeholder="Clave"
                                required="required">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block">Iniciar Sesión</button>
                        </div>
                    </form>
                    <div class="d-flex justify-content-center links">
                        <a class="btn btn-dark btn-block"
                            href="index.php?pid=<?php echo base64_encode("presentacion/registrarse.php")?>">Registrarse</a>
                    </div>
					<br>
                    <p>
                        <a href="#">¿Olvido su clave?</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

