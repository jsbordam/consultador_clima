<?php
    //Para recuperar la variable sesion cuando se recarga la pagina:
    session_start();

    //Para importar las clases que necesito usar:
    require "logica/Usuario.php";
    require "logica/Mensaje.php";
    require "persistencia/Conexion.php";
    
    //Zona horaria en colombia:
    date_default_timezone_set('America/Bogota'); 

    //Para cerrar la session:
    if (isset($_GET["session"]) && $_GET["session"] == 0)
    {
        $_SESSION["id"] = null; //Se vacia el objeto de session, para romper esa session.
    }
    
    //Este algoritmo es para que me muestre los errores
    //en tiempo de ejecución:
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    ////////////////////////////////////////////////////

    //Pid es una variable para redireccionar de una a otra pagina:
    $pid=NULL;
    if (isset($_GET["pid"]))
    {
        //En pid se guarda la ruta decodificada:
        $pid = base64_decode($_GET["pid"]);
    }
?>

<!doctype html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <!-- Esta linea es para el resposive = ajustar el contenido al tamaño de la ventana -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    <!-- Esta linea es para implementar: Font Awesome (iconos) -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
    <!-- Esta linea es para implementar: Bootstrap y Bootswatch (Temas) -->
    <link href="https://bootswatch.com/4/united/bootstrap.css" rel="stylesheet" />
    <!-- Lineas importantes: -->
    <link rel="stylesheet" type="text/css"
        href="//fonts.googleapis.com/css?family=Dancing+Script:700%7CRaleway:300,400,600,700,900">

    <!-- Bootstrap CSS File -->
    <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Libraries CSS Files -->
    <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="lib/animate-css/animate.min.css" rel="stylesheet">
    <!-- Main Stylesheet File -->
    <link href="css/style.css" rel="stylesheet">


    <!-- Estas lineas son para implementar: jQuery, Popper.js y Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js">
    </script>
    <title>Tiempo en el mundo</title>
    <link rel="icon" type="image/png" href="img/icono.png">
    <!-- Estas lineas son para implementar tooltip (Pequeñas etiquetas descriptivas de botones) -->
    <script type="text/javascript">
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
    </script>
</head>

<body>
    <?php 
    	   //Para validar si existe session:
    	   if (isset($pid))
    	   {
    	       if (isset($_SESSION["id"]))//Si la sesion existe entonces entra
    	       {
    	           if ($_SESSION["rol"] == "Administrador")//El rol de esa sesion es Admin
    	           {
    	               include "presentacion/administrador/menuAdministrador.php";
    	               include $pid;
    	           }
    	           else //El rol de esa sesion es Cliente
    	           {
    	               include "presentacion/cliente/menuCliente.php";
    	               include $pid;
    	           }  	           
    	       }   	      
    	       else 
    	       {
                    include $pid; 
    	       }
    	   }
    	   else 
    	   {
    	       include "presentacion/inicio.php";
    	   }
    	?>
</body>

</html>