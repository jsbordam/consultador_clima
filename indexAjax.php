<!-- Estas lineas es para implementar tooltip (Pequeñas etiquetas descriptivas de botones) -->
<script type="text/javascript">
	$(function () 
	{
      $('[data-toggle="tooltip"]').tooltip()
    })
</script>   
<?php  
    //Para recuperar la variable sesion cuando se recarga la pagina:
    session_start();
    require "logica/Usuario.php";
    require "logica/Mensaje.php";
    require "persistencia/Conexion.php";
    
    //Desencriptamos el pid para que lo pueda leer:
    $pid = base64_decode($_GET["pid"]);
    include $pid;
    
?>