<?php
    class UsuarioDAO
    {
        private $idUsuario;
        private $nombre;
        private $apellido;
        private $tipo_docto;
        private $num_docto;
        private $correo;
        private $clave;
        private $estado;
        private $foto;
        private $idRol;
     
        
        //Constructor:
        
        function UsuarioDAO($pIdUsuario="", $pNombre="", $pApellido="", $pTipo_docto="", $pNum_docto="", $pCorreo="", $pClave="", $pEstado="", $pFoto="", $pIdRol="")
        {
            $this -> idUsuario = $pIdUsuario;
            $this -> nombre = $pNombre;
            $this -> apellido = $pApellido;
            $this -> tipo_docto = $pTipo_docto;
            $this -> num_docto = $pNum_docto;
            $this -> correo = $pCorreo;
            $this -> clave = $pClave;
            $this -> estado = $pEstado;
            $this -> foto = $pFoto;
            $this -> idRol = $pIdRol;
        }
        
        //Metodos para las consultas en la BD:
        
        
        //Para crear:
        function crear()
        {
            return "INSERT into usuario (nombre, apellido, tipo_docto, num_docto, correo, clave, estado, foto, idRol)
                    VALUES ('" . $this -> nombre . "', '" . $this -> apellido . "', '" . $this -> tipo_docto . "', '" . $this -> num_docto . "', '" . $this -> correo . "', '" . $this -> clave . "', '" . $this -> estado . "', '" . $this -> foto . "', '" . $this -> idRol . "')";
        }      
        
        //Para autenticar:
        function autenticar()
        {
            return "SELECT idUsuario, estado, idRol
                    FROM usuario
                    WHERE correo = '" . $this -> correo . "' and clave = md5('" . $this -> clave . "')";
        }
        
        //Para consultar uno:
        function consultar()
        {
            return "SELECT nombre, apellido, tipo_docto, num_docto, correo, clave, estado, foto, idRol
                    FROM usuario
                    WHERE idUsuario = '" . $this -> idUsuario . "'";
        }
        
        //Para consultar todos los usuarios de un rol especifico:
        function consultarTodos()
        {
            return "SELECT idUsuario, nombre, apellido, tipo_docto, num_docto, correo, estado, foto, idRol
                    FROM usuario";
        }
        
        //Para cambiar estado de un usuario:
        function cambiarEstado($estado)
        {
            return "UPDATE usuario
                    SET estado = '". $estado ."'
                    WHERE idUsuario = '" . $this -> idUsuario ."'";
        }  
        
        //Para editar un usuario:
        function editar()
        {
            return "UPDATE usuario
                    SET nombre = '". $this->nombre . "', apellido = '". $this->apellido . "', tipo_docto = '" . $this->tipo_docto . "', num_docto = '" . $this->num_docto . "', clave = '" . $this->clave ."', estado = '" . $this->estado ."', foto = '". $this -> foto . "', idRol = '" . $this->idRol . "'
                    WHERE idUsuario = '" . $this->idUsuario . "'";
        }

        //Para eliminar un usuario:
        function eliminar()
        {
            return "DELETE FROM usuario                
                    WHERE idUsuario = '" . $this->idUsuario . "'";
        }
    }
?>
