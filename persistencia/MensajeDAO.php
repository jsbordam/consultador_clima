<?php
    class MensajeDAO
    {
        private $idMensaje;
        private $mensaje;
        private $fecha;
        private $idUsuario;
     
        
        //Constructor:
        
        function MensajeDAO($pIdMensaje="", $pMensaje="", $pFecha="", $pIdUsuario="") 
        {
            $this -> idMensaje = $pIdMensaje;
            $this -> mensaje = $pMensaje;
            $this -> fecha = $pFecha;
            $this -> idUsuario = $pIdUsuario;
        }
        
        //Metodos para las consultas en la BD:
        
        
        //Para crear:
        function crear()
        {
            return "INSERT into mensaje (mensaje, fecha, idUsuario)
                    VALUES ('" . $this -> mensaje . "', '" . $this -> fecha . "', '" . $this -> idUsuario . "')";
        }      
        

        //Para consultar todos los mensajes:
        function consultarTodos()
        {
            return "SELECT idMensaje, mensaje, fecha, idUsuario
                    FROM mensaje
                    ORDER BY idMensaje DESC";
        }
    }
?>
