<?php
    class Conexion
    {
        private $mysqli;
        private $resultado;
        
        //Funcion para abrir la conexion:
        function abrir ()
        {
            $this -> mysqli = new mysqli("localhost:3307", "root", "", "netgrid_bd");
            $this -> mysqli -> set_charset ("utf8"); //Para que lea todos los caracteres
        }
        
        //Funcion para cerrar la conexion:
        function cerrar ()
        {
            $this -> mysqli -> close();
        }
        
        //Funcion para ejecutar sentencias:
        function ejecutar ($sentencia)
        {
            $this -> resultado = $this -> mysqli -> query($sentencia); 
        }
        
        //Funcion para extraer el siguiente registro, iterar la conexion:
        function extraer ()
        {
            return $this -> resultado -> fetch_row();
        }
        
        //Funcion para devolver numero de filas del resultado de la sentencia:
        function numFilas ()
        {
            //Condicional ternario:
            return ($this -> resultado != null) ? $this -> resultado -> num_rows : 0;
        }        
    } 
?>