<?php
    //Para importar las clases que necesito usar:
    require "persistencia/UsuarioDAO.php";
    
    class Usuario 
    {
        private $idUsuario;
        private $nombre;
        private $apellido;
        private $tipo_docto;
        private $num_docto;
        private $correo;
        private $clave;
        private $estado;
        private $foto;
        private $idRol;
        
        private $conexion;
        private $usuarioDAO;
        
        
    
        //Constructor:
        
        function Usuario($pIdUsuario="", $pNombre="", $pApellido="", $pTipo_docto="", $pNum_docto="", $pCorreo="", $pClave="", $pEstado="", $pFoto="", $pIdRol="") 
        {
            $this -> idUsuario = $pIdUsuario;
            $this -> nombre = $pNombre;
            $this -> apellido = $pApellido;
            $this -> tipo_docto = $pTipo_docto;
            $this -> num_docto = $pNum_docto;
            $this -> correo = $pCorreo;
            $this -> clave = $pClave;
            $this -> estado = $pEstado;
            $this -> foto = $pFoto;
            $this -> idRol = $pIdRol;
            $this -> conexion = new Conexion();
            $this -> usuarioDAO = new UsuarioDAO($pIdUsuario, $pNombre, $pApellido, $pTipo_docto, $pNum_docto, $pCorreo, $pClave, $pEstado, $pFoto, $pIdRol);
        }
        
        
        //Metodos GET:
        
        public function getIdUsuario()
        {
            return $this->idUsuario;
        }
        
        public function getNombre()
        {
            return $this->nombre;
        }
        
        public function getApellido()
        {
            return $this->apellido;
        }
        
        public function getTipo_docto()
        {
            return $this->tipo_docto;
        }
        
        public function getNum_docto()
        {
            return $this->num_docto;
        }
        
        public function getCorreo()
        {
            return $this->correo;
        }
        
        public function getClave()
        {
            return $this->clave;
        }
        
        public function getEstado()
        {
            return $this->estado;
        }
        
        public function getFoto()
        {
            return $this->foto;
        }
        
        public function getIdRol()
        {
            return $this->idRol;
        }
             
        //Metodos para conectar con las consultas de la BD:
        
        
        //Metodo para crear un usuario:
        function crear()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> usuarioDAO -> crear();
            $this -> conexion -> ejecutar($this -> usuarioDAO -> crear());
            $this -> conexion -> cerrar(); //Se cierra la conexion      
        }
        
        
        //Metodo para autenticar:
        function autenticar()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> usuarioDAO -> autenticar();
            $this -> conexion -> ejecutar($this -> usuarioDAO -> autenticar());
            $this -> conexion -> cerrar(); //Se cierra la conexion

            
            //Para saber si el usuario y contraseña son correctos:
            if ($this -> conexion -> numFilas() == 1)//Quiere decir que es correcto y encontro el usuario en la BD
            {  
                //Fetch_row me trae un arreglo de objetos, como sé que el
                //arreglo que me trae solo tiene una fila porque es
                //el id y el estado, entonces le indico la posicion [0] y [1]:
                $resultado = $this -> conexion -> extraer();
                
                $this -> idUsuario = $resultado[0];
                $this -> estado = $resultado[1];
                $this -> idRol = $resultado[2]; 

                return true; //Puede entrar                 
            }
            else //No encontro el correo y clave en la BD
            {
                return false;
            }
        }
        
        //Metodo para consultar un usuario:
        function consultar()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> usuarioDAO -> consultar();
            $this -> conexion -> ejecutar($this -> usuarioDAO -> consultar());
            $this -> conexion -> cerrar(); //Se cierra la conexion

            if ($this -> conexion -> numFilas() == 0)//No existe el usuario
            {

            }
            else
            {
                $resultado = $this -> conexion -> extraer();
                $this -> nombre = $resultado[0];
                $this -> apellido = $resultado[1];
                $this -> tipo_docto = $resultado[2];
                $this -> num_docto = $resultado[3];
                $this -> correo = $resultado[4];
                $this -> clave = $resultado[5];  
                $this -> estado = $resultado[6];
                $this -> foto = $resultado[7];
                $this -> idRol = $resultado[8];

            }    
        }
        
        //Metodo para consultar todos los usuarios:
        function consultarTodos()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql =  echo $this -> usuarioDAO -> consultarTodos();
            $this -> conexion -> ejecutar($this -> usuarioDAO -> consultarTodos());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            $usuarios = array();
            //Para que itere tantas veces como hayan registros:
            while (($resultado = $this -> conexion -> extraer()) != null)
            {
                array_push($usuarios, new Usuario ($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5], "", $resultado[6], $resultado[7], $resultado[8]));
            }
            return $usuarios;
        }
        
        //Metodo para cambiar estado de un usuario:
        function cambiarEstado($estado)
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> usuarioDAO -> cambiarEstado($estado);
            $this -> conexion -> ejecutar($this -> usuarioDAO -> cambiarEstado($estado));
            $this -> conexion -> cerrar(); //Se cierra la conexion
        }

        //Metodo para editar un usuario:
        function editar()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> usuarioDAO -> editar();
            $this -> conexion -> ejecutar($this -> usuarioDAO -> editar());
            $this -> conexion -> cerrar(); //Se cierra la conexion
        }

        //Metodo para eliminar un usuario:
        function eliminar()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> usuarioDAO -> eliminar();
            $this -> conexion -> ejecutar($this -> usuarioDAO -> eliminar());
            $this -> conexion -> cerrar(); //Se cierra la conexion
        }   
    }
?>