<?php
    //Para importar las clases que necesito usar:
    require "persistencia/MensajeDAO.php";
    
    class Mensaje 
    {
        private $idMensaje;
        private $mensaje;
        private $fecha;
        private $idUsuario;
        
        
        private $conexion;
        private $mensajeDAO;
        
        
    
        //Constructor:
        
        function Mensaje ($pIdMensaje="", $pMensaje="", $pFecha="", $pIdUsuario="") 
        {
            $this -> idMensaje = $pIdMensaje;
            $this -> mensaje = $pMensaje;
            $this -> fecha = $pFecha;
            $this -> idUsuario = $pIdUsuario;
            $this -> conexion = new Conexion();
            $this -> mensajeDAO = new MensajeDAO($pIdMensaje, $pMensaje, $pFecha, $pIdUsuario);
        }
        
        
        //Metodos GET:
        
        public function getIdMensaje()
        {
            return $this->idMensaje;
        }
        
        public function getMensaje()
        {
            return $this->mensaje;
        }

        public function getFecha()
        {
            return $this->fecha;
        }

        public function getIdUsuario()
        {
            return $this->idUsuario;
        }
        
        
             
        //Metodos para conectar con las consultas de la BD:
        
        
        //Metodo para guardar un mensaje:
        function crear()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> mensajeDAO -> crear();
            $this -> conexion -> ejecutar($this -> mensajeDAO -> crear());
            $this -> conexion -> cerrar(); //Se cierra la conexion      
        }
        
        
        //Metodo para consultar todos los mensajes:
        function consultarTodos()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql =  echo $this -> mensajeDAO -> consultarTodos();
            $this -> conexion -> ejecutar($this -> mensajeDAO -> consultarTodos());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            $mensajes = array();
            //Para que itere tantas veces como hayan registros:
            while (($resultado = $this -> conexion -> extraer()) != null)
            {
                array_push($mensajes, new Mensaje ($resultado[0], $resultado[1], $resultado[2], $resultado[3]));
            }
            return $mensajes;
        }
    }
?>